#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	
	// ofSetVerticalSync(false);
	ofSetLogLevel(OF_LOG_NOTICE);
	
	numRegios = 4;
	regios.resize(numRegios, ofRectangle(0,0,1,1));
	
	densityWidth = 1920;
	densityHeight = 1080;
	simulationWidth = densityWidth;
	simulationHeight = densityHeight;
	// process the average on 16th resolution
	avgWidth = densityWidth / 4;
	avgHeight = densityHeight / 4;
	windowWidth = ofGetWindowWidth();
	windowHeight = ofGetWindowHeight();
	
	opticalFlow.setup(simulationWidth, simulationHeight);
	velocityBridgeFlow.setup(simulationWidth, simulationHeight);
	densityBridgeFlow.setup(simulationWidth, simulationHeight, densityWidth, densityHeight);
	temperatureBridgeFlow.setup(simulationWidth, simulationHeight);
	fluidFlow.setup(simulationWidth, simulationHeight, densityWidth, densityHeight);
	densityMouseFlow.setup(densityWidth, densityHeight, FT_DENSITY);
	velocityMouseFlow.setup(simulationWidth, simulationHeight, FT_VELOCITY);
	pixelFlow.setup(avgWidth, avgHeight, FT_VELOCITY);
	averageFlows.resize(numRegios);
	for (int i=0; i<numRegios; i++) {
		averageFlows[i].setup(avgWidth, avgHeight, FT_VELOCITY);
		averageFlows[i].setRoi(1.0 / (numRegios + 1.0) * (i + 1), .2, 1.0 / (numRegios + 2.0), .6);
	}
	
	flows.push_back(&opticalFlow);
	flows.push_back(&velocityBridgeFlow);
	flows.push_back(&densityBridgeFlow);
	flows.push_back(&temperatureBridgeFlow);
	flows.push_back(&fluidFlow);
	flows.push_back(&densityMouseFlow);
	flows.push_back(&velocityMouseFlow);
	for (auto& f : averageFlows) { flows.push_back(&f); }
	
	for (auto flow : flows) { flow->setVisualizationFieldSize(glm::vec2(simulationWidth / 2, simulationHeight / 2)); }
	
	mouseFlows.push_back(&densityMouseFlow);
	mouseFlows.push_back(&velocityMouseFlow);
	
	simpleCam.setup(densityWidth, densityHeight, true);
	cameraFbo.allocate(densityWidth, densityHeight);
	ftUtil::zero(cameraFbo);
	
	
}

//--------------------------------------------------------------
void ofApp::update(){
	float dt = 1.0 / max(ofGetFrameRate(), 1.f); // more smooth as 'real' deltaTime.
	
	simpleCam.update();
	if (simpleCam.isFrameNew()) {
		cameraFbo.begin();
		simpleCam.draw(cameraFbo.getWidth(), 0, -cameraFbo.getWidth(), cameraFbo.getHeight());  // draw flipped
		cameraFbo.end();
		
		opticalFlow.setInput(cameraFbo.getTexture());
	}
	
	for (auto flow: mouseFlows) { flow->update(dt); }

	opticalFlow.update();
	
	velocityBridgeFlow.setVelocity(opticalFlow.getVelocity());
	velocityBridgeFlow.update(dt);
	densityBridgeFlow.setDensity(cameraFbo.getTexture());
	densityBridgeFlow.setVelocity(opticalFlow.getVelocity());
	densityBridgeFlow.update(dt);
	temperatureBridgeFlow.setDensity(cameraFbo.getTexture());
	temperatureBridgeFlow.setVelocity(opticalFlow.getVelocity());
	temperatureBridgeFlow.update(dt);
	
	fluidFlow.addVelocity(velocityBridgeFlow.getVelocity());
	fluidFlow.addDensity(densityBridgeFlow.getDensity());
	fluidFlow.addTemperature(temperatureBridgeFlow.getTemperature());
	for (auto flow: mouseFlows) { if (flow->didChange()) { fluidFlow.addFlow(flow->getType(), flow->getTexture()); } }
	fluidFlow.update(dt);
	
	pixelFlow.setInput(opticalFlow.getVelocity());
	for (auto flow: mouseFlows) { if (flow->didChange() && flow->getType() == FT_VELOCITY) { pixelFlow.addInput(flow->getTexture()); } }
	pixelFlow.update();
	for (auto& f : averageFlows) { f.update(pixelFlow.getPixels()); }
}

//--------------------------------------------------------------
void ofApp::draw(){
	
	ofClear(0,0);
	
	ofPushStyle();
	if (toggleCameraDraw.get()) {
		ofEnableBlendMode(OF_BLENDMODE_DISABLED);
		cameraFbo.draw(0, 0, windowWidth, windowHeight);
	}
	
	ofEnableBlendMode(OF_BLENDMODE_ALPHA);
	switch(visualizationMode.get()) {
		case FLUID_DEN:		fluidFlow.draw(0, 0, windowWidth, windowHeight); break;
		default: break;
	}
	
	if (toggleMouseDraw) {
		ofEnableBlendMode(OF_BLENDMODE_ALPHA);
		densityMouseFlow.draw(0, 0, windowWidth, windowHeight);
		velocityMouseFlow.draw(0, 0, windowWidth, windowHeight);
	}
	
	if (toggleAverageDraw) {
		ofEnableBlendMode(OF_BLENDMODE_ALPHA);
		for (auto& f: averageFlows) { f.draw(0, 0, windowWidth, windowHeight); };
	}
	
	ofPopStyle();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch (key) {
		default: break;
        case '0': visualizationMode.set(FLUID_DEN); break;
			break;
	}
}

//--------------------------------------------------------------
void ofApp::toggleResetListener(bool& _value) {
	if (_value) {
		for (auto flow : flows) { flow->reset(); }
	}
	_value = false;
}
