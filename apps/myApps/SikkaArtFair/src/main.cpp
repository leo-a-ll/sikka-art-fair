#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	
    ofGLFWWindowSettings windowSettings;
#ifdef USE_PROGRAMMABLE_GL
    windowSettings.setGLVersion(4, 1);
#endif
	windowSettings.setSize(1920, 1080);
    // windowSettings.windowMode = OF_WINDOW;
    windowSettings.windowMode = OF_FULLSCREEN;
    
    ofCreateWindow(windowSettings);
    
    ofRunApp(new ofApp());
}
